<?php
get_header();
$class                = welldone_main_container_class();
$terms                = get_terms( [
	'taxonomy'   => 'projects_category',
	'hide_empty' => true,
] );
$quantity_terms       = get_queried_object_id();
$quantity_term_object = get_term_by( 'id', $quantity_terms, 'projects_category' );
$quantity_term_name   = $quantity_term_object->name;
$alternative_heading  = get_term_meta( $quantity_terms, 'alternative_heading', true );
$header               = ! empty( $alternative_heading ) ? $alternative_heading : $quantity_term_name;

?>
<div id="pageContent">
    <div class="<?php echo esc_attr( $class ); ?>">

        <div class="row">
            <div class="header-cat-desc">
                <h2><?php echo $header; ?></h2>
				<?php echo term_description( $term_id ); ?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="box">
                <h3>CATEGORY<span class="expand">+</span></h3>
                <ul>
					<?php
					foreach ( $terms as $term ): ?>
                        <a href="<?php echo get_term_link( $term->term_id ); ?>">
                            <li><?php echo $term->name; ?> ( <?php echo $term->count; ?> )</li>
                        </a>
					<?php endforeach; ?>
                </ul>
            </div>
            <hr>
            <div class="box">
                <h3>CHARITY<span class="expand">+</span></h3>
                <ul>
					<?php
					$terms_charity = get_terms( [
						'taxonomy'   => 'projects_charity',
						'hide_empty' => true,
					] ); ?>
					<?php foreach ( $terms_charity as $term ): ?>
                        <a href="<?php echo get_term_link( $term->term_id ); ?>">
                            <li><?php echo $term->name; ?> ( <?php echo $term->count; ?> )</li>
                        </a>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="col-sm-9">
            <div class="in-cat-project-page">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <article>
						<?php
						$a = new WP_PWP;
						?>
                        <div>
                            <img width="200" height="210" src="<?php echo $a->get_default_thumbail( 2 ); ?>"/>
                        </div>
                        <div class="project-content">
                            <h2><?php the_title(); ?></h2>
                            <p><?php
								$content         = get_the_content();
								$trimmed_content = wp_trim_words( $content, 40 );
								echo $trimmed_content;
								?></p>
							<?php $donate_url = get_post_meta( get_the_ID(), '_wppwp_donate_url', true ); ?>
                            <a href="<?php echo $donate_url; ?>">
                                <button style="margin-top: 1em; width: 140px;height: 40px;background-color: rgba(22, 155, 213, 1);color: white;border: none;border-radius: 5px">
                                    DONATE NOW!
                                </button>
                            </a>
                            <div class="row">
                                <a href="<?php the_permalink(); ?>">
                                    <div style="float: right;margin-top: 5%;">VIEW DETAIL ></div>
                                </a>
                            </div>
                        </div>
                    </article>
				<?php endwhile; ?>
                    <div class="wppwp-paginations">
                        <div class="nav-previous alignleft"><?php next_posts_link( '< Prev. Page' ); ?></div>
                        <div class="nav-next alignright"><?php previous_posts_link( 'Next Page >' ); ?></div>
                    </div>
				<?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
            </div>
        </div>

    </div>
</div>

<?php get_footer(); ?>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("h3 span").click(function () {
            jQuery(this).parent().next().slideToggle();
        });
    });
</script>



