<?php

$title                   = get_the_title();
$content                 = get_the_content();
$a                       = new WP_PWP;

$image               = ! empty( get_the_post_thumbnail_url( get_the_ID() ) ) ? get_the_post_thumbnail_url( get_the_ID() ) : $a->get_default_thumbail( 1 );
$custom_title        = get_post_meta( get_the_ID(), '_wppwp_custom_title', true );
$custom_descriptions = get_post_meta( get_the_ID(), '_wppwp_custom_descriptions', true );
$wppwp_start         = get_post_meta( get_the_ID(), '_wppwp_start_at_formated', true );

$wppwp_finish = get_post_meta( get_the_ID(), '_wppwp_finish_at_formated', true );


$funds_raised = get_post_meta( get_the_ID(), '_wppwp_funds_raised', true );
$goal         = get_post_meta( get_the_ID(), '_wppwp_goal', true );
$status       = get_post_meta( get_the_ID(), '_wppwp_status', true );
$donate_url   = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );


$json_campany = get_post_meta( get_the_ID(), '_wppwp_json_campany', true );

?>

<?php get_header(); ?>

    <div class="row" style="margin-left: 10%; margin-right: 10%; margin-top: 5%; margin-bottom: 5%;">
        <div class="col-md-3">
			<?php if ( ! empty( $image ) ) : ?>
                <img src="<?php echo $image; ?> ">
			<?php else : echo 'No image upload'; ?>
			<?php endif; ?>
        </div>
        <div class="col-md-5 center-block-content" style="min-height: 410px">
            <h2><?php echo $title; ?></h2>
            <p><?php echo apply_filters( 'post_content', $post->post_content ); ?> </p>
            <a href="<?php echo $donate_url; ?>">
                <button style="position: absolute;bottom: 0; width: 140px;height: 40px;background-color: rgba(22, 155, 213, 1);color: white;border: none;border-radius: 5px">
                    DONATE NOW!
                </button>
            </a>

        </div>
        <div class="col-md-4">
            <style type="text/css">
                table {
                    border: solid 1px black;
                    width: 270px;
                    height: 347px;
                }

                .tg-yw4l {
                    border-top-color: black;
                    padding-left: 5%;
                }

                #th-td-desc {
                    font-weight: 100 !important;
                    text-transform: none !important;
                }

                /*.tg  {border-collapse:collapse;border-spacing:0;}*/
                /*.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}*/
                /*.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}*/
                /*.tg .tg-yw4l{vertical-align:top}*/
            </style>
            <table class="tg">
                <h2><span><?php echo $custom_title; ?></span></h2>
                <tr>
                    <th class="tg-yw4l" id="th-td-desc" colspan="2">
                        <span><?php echo $post->post_excerpt;?></span>
                    </th>
                </tr>
                <tr>
                    <td class="tg-yw4l">
                        START:
                    </td>
                    <td class="tg-yw4l">
						<?php echo $wppwp_start ?>
                    </td>
                </tr>
                <tr>
                    <td class="tg-yw4l">
                        FINISH:
                    </td>
                    <td class="tg-yw4l">
						<?php echo $wppwp_finish ?>
                    </td>
                </tr>
                <tr>
                    <td class="tg-yw4l">
                        FUNDS RAISED:
                    </td>
                    <td class="tg-yw4l">
						<?php echo $funds_raised->currency->symbol . number_format( $funds_raised->cents / 100, 2, '.', ' ' ); ?>
                    </td>
                </tr>
                <tr>
                    <td class="tg-yw4l">
                        GOAL:
                    </td>
                    <td class="tg-yw4l">
						<?php if ( ! empty( $goal ) ):
							echo $funds_raised->currency->symbol . $goal;
						endif;
						?>
                    </td>
                </tr>
            </table>
        </div>

    </div>


<?php get_footer() ?>