<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class PioGroup_Wp_Autoloader {

	private $dirs = array();
	protected $plugin_path;

	public function __construct( $plugin_path ) {
		$this->plugin_path = $plugin_path;
	}

	public function load( $class ) {
		foreach ( $this->dirs as $dir ) {
			$path = $this->plugin_path . '/'. $dir . '/' . $this->get_class_filename( $class );
			$this->get_class_path( $class, $path );
		}
	}

	protected function get_class_filename( $class ) {
		$class = strtolower( $class );

		return 'class-' . str_replace( '_', '-', $class ) . '.php';
	}

	protected function get_class_path( $class, $path ) {
		if ( file_exists( $path ) ) {
			include $path;
		}
	}

	public function autoload( $dir ) {
		$this->add_dir( $dir );
		spl_autoload_register( array( $this, 'load' ) );
	}

	protected function add_dir( $dir ) {
		if( ! in_array( $dir, $this->dirs ) ) {
			array_push( $this->dirs, $dir );
		}
	}

}

