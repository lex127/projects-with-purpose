<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

