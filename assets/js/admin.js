jQuery(document).ready(function () { // wait for page to finish loading

    jQuery("#wppwpGiveAll").click(function () {

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_get_all_company_callback',
                variable: 45 // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                console.log(data);
            }
        });

    });

    jQuery("#wppwpUpdateMeta").click(function () {

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_update_meta_company_callback',
                variable: 45 // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                console.log(data);
            }
        });

    });

    jQuery("#wppwp_update_single_company").click(function () {

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_update_single_company_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                params: options
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {

            }
        });

    });


});