<?php
/**
 * Plugin Name: Projects with Purpose
 * Description: Projects with Purpose
 * Version: 0.1
 * Author: Piogroup
 * Author URI:
 * Text Domain: projects-with-purpose
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
define( 'WPPWP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPPWP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if ( ! class_exists( 'WP_PWP' ) ) :

	/**
	 * Main Class.
	 */
	final class WP_PWP {

		const ADMIN_PATH = 'admin';
		const PREFIX = 'wppwp_';
		const INCLUDE_PATH = 'includes';
		public static $table_name = false;


		/**
		 * The single instance of the class.
		 */
		protected static $_instance = null;

		protected $loader;

		protected $post_type;

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Cloning is forbidden.
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-pwp' ), '2.1' );
		}

		/**
		 * Unserializing instances of this class is forbidden.
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-pwp' ), '2.1' );
		}

		public function __construct() {
			global $wpdb;

			self::$table_name = $wpdb->prefix . 'wppwp';
			$this->add_hooks();
			$this->includes();
			$this->load();


			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );

		}

		public function add_hooks() {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_resources' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'load_resources' ) );
		}

		public function includes() {
			include_once( 'vendor/piogroup/piogroup-wp-autoloader.php' );
			include_once( 'vendor/piogroup/piogroup-wp-template.php' );
			include_once( 'vendor/piogroup/piogroup-wp-cpt.php' );
			include_once( 'includes/fields/taxonomy-field.php' );
		}

		public function load() {
			include_once( 'includes/cpt/projects.php' );
			include_once( 'admin/project-settings.php' );
			include_once( 'includes/projects-organiser-templates.php' );
			$this->loader = new PioGroup_Wp_Autoloader( $this->get_path() );
			if ( $this->is_request( 'admin' ) ) {
				$this->loader->autoload( self::ADMIN_PATH );
			}
			$this->loader->autoload( self::INCLUDE_PATH );

		}

		/**
		 * What type of request is this?
		 *
		 * @param  string $type admin, ajax, cron or frontend.
		 *
		 * @return bool
		 */
		public function is_request( $type ) {
			switch ( $type ) {
				case 'admin' :
					return is_admin();
				case 'ajax' :
					return defined( 'DOING_AJAX' );
				case 'frontend' :
					return ( ! is_admin() || defined( 'DOING_AJAX' ) );
			}
		}

		//TODO refactoring

		public function get_default_thumbail( $image = 1 ) {
			if ( $image == 2 ) {
				return untrailingslashit( plugins_url( '/', __FILE__ ) . 'templates/images/empty-image-cat.png' );
			}

			return untrailingslashit( plugins_url( '/', __FILE__ ) . 'templates/images/empty-image.png' );
		}

		/**
		 * Get the plugin url.
		 * @return string
		 */
		public function get_url() {
			return untrailingslashit( plugins_url( '/', __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 * @return string
		 */
		public function get_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		/**
		 * Get the template path.
		 * @return string
		 */
		public function get_template_path() {
			return apply_filters( 'woocommerce_template_path', 'wc_related_products/' );
		}

		/**
		 * Get Ajax URL.
		 * @return string
		 */
		public function ajax_url() {
			return admin_url( 'admin-ajax.php', 'relative' );
		}


		/**
		 * Load localization files.
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'wp-pwp', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}

		public static function load_resources() {

			wp_register_script(
				self::PREFIX . 'admin-js',
				plugins_url( 'assets/js/admin.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);
			wp_register_style(
				self::PREFIX . 'main-css',
				plugins_url( 'assets/css/main.css', __FILE__ ),
				array(),
				'1.0.0',
				'all'
			);

			if ( is_admin() ) {
				wp_enqueue_script( self::PREFIX . 'admin-js' );
			}
			if ( is_tax( 'projects_category' ) || is_tax( 'projects_charity' ) || is_singular( 'wppwp_projects' ) ) {
				wp_enqueue_style( self::PREFIX . 'main-css' );
			}

		}

	}
endif;

function WP_PWP() {
	register_activation_hook( __FILE__, 'wppwp_activate' );

	register_activation_hook( __FILE__, 'wppwp_cron_settings' );

	return WP_PWP::instance();
}

WP_PWP();


function wppwp_cron_settings() {
	wp_clear_scheduled_hook( 'wppwp_get_post_api' );


	wp_schedule_event( time(), 'hourly', 'wppwp_get_post_api' );
	if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
		add_action( 'wppwp_get_post_api', 'wp_ajax_wppwp_get_all_company_callback' );

	}
}

register_deactivation_hook( __FILE__, 'wppwp_deactivation' );

function wppwp_deactivation() {
	wp_clear_scheduled_hook( 'my_hourly_event' );
}

function wppwp_activate() {

	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = WP_PWP::$table_name;

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id bigint(20) NOT NULL AUTO_INCREMENT,
        first_page bigint(20) NOT NULL DEFAULT '1',
        limit_company bigint(10) NOT NULL DEFAULT '50',
        last_update datetime DEFAULT '0000-00-00 00:00:00',
        created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        created_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY  (id)
        ) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

}