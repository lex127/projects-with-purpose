<?php
/**
 * Checks if provided template path points to an 'event' template recognised by EO, given the context.
 * This will one day ignore context, and if only the event archive template is present in theme folder
 * it will use that  regardless. If no event-archive tempate is present the plug-in will pick the most appropriate
 * option, first from the theme/child-theme directory then the plugin.
 *
 * @ignore
 * @since 1.3.1
 *
 * @param string $templatePath absolute path to template or filename (with .php extension)
 * @param string $context What the template is for ('event','archive-event','event-venue', etc).
 * @return (true|false) return true if template is recognised as an 'event' template. False otherwise.
 */
function organiser_is_project_template($templatePath,$context=''){
	$template = basename($templatePath);
	switch($context):
		case 'wppwp_projects';
			return $template == 'single-event.php';
		case 'archive':
			return $template == 'archive-event.php';
		case 'event-venue':
			if((1 == preg_match('/^taxonomy-event-venue((-(\S*))?).php/',$template) || $template == 'venues-template.php'))
				return true;
			return false;
		case 'projects_category':
			return (1 == preg_match('/^projects-category((-(\S*))?).php/',$template));
		case 'projects_charity':
			return (1 == preg_match('/^projects-charity((-(\S*))?).php/',$template));
		case 'event-tag':
			return (1 == preg_match('/^taxonomy-event-tag((-(\S*))?).php/',$template));
	endswitch;
	return false;
}
/**
 * Checks to see if appropriate templates are present in active template directory.
 * Otherwises uses templates present in plugin's template directory.
 * Hooked onto template_include'
 *
 * @ignore
 * @since 1.0.0
 * @param string $template Absolute path to template
 * @return string Absolute path to template
 */
function projectsorganiser_set_template( $template ){

	if( is_singular('wppwp_projects') && !organiser_is_project_template($template,'wppwp_projects'))
		$template = WPPWP_PLUGIN_DIR.'templates/single-project-template.php';

	if( is_tax('projects_category')  && !organiser_is_project_template($template,'projects_category'))
		$template = WPPWP_PLUGIN_DIR.'templates/projects-category.php';

	if( is_tax('projects_charity')  && !organiser_is_project_template($template,'projects_charity'))
		$template = WPPWP_PLUGIN_DIR.'templates/projects-charity.php';

	return $template;
}
add_filter('template_include', 'projectsorganiser_set_template');
?>
