<?php
function projects_category_add_meta_fields( $taxonomy ) {
	?>
	<div class="form-field term-group">
		<label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
		<input type="text" id="alternative_heading" name="alternative_heading"/>
	</div>
	<?php
}

add_action( 'projects_category_add_form_fields', 'projects_category_add_meta_fields', 10, 2 );

function projects_category_edit_meta_fields( $term, $taxonomy ) {
	$alternative_heading = get_term_meta( $term->term_id, 'alternative_heading', true );
	?>
	<tr class="form-field term-group-wrap">
		<th scope="row">
			<label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
		</th>
		<td>
			<input type="text" id="alternative_heading" name="alternative_heading"
			       value="<?php echo $alternative_heading; ?>"/>
		</td>
	</tr>
	<?php
}

add_action( 'projects_category_edit_form_fields', 'projects_category_edit_meta_fields', 10, 2 );

function projects_category_save_taxonomy_meta( $term_id, $tag_id ) {
	if ( isset( $_POST['alternative_heading'] ) ) {
		update_term_meta( $term_id, 'alternative_heading', esc_attr( $_POST['alternative_heading'] ) );
	}
}

add_action( 'created_projects_category', 'projects_category_save_taxonomy_meta', 10, 2 );
add_action( 'edited_projects_category', 'projects_category_save_taxonomy_meta', 10, 2 );


function projects_charity_add_meta_fields( $taxonomy ) {
	?>
	<div class="form-field term-group">
		<label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
		<input type="text" id="alternative_heading" name="alternative_heading"/>
	</div>
	<?php
}

add_action( 'projects_charity_add_form_fields', 'projects_charity_add_meta_fields', 10, 2 );

function projects_charity_edit_meta_fields( $term, $taxonomy ) {
	$alternative_heading = get_term_meta( $term->term_id, 'alternative_heading', true );
	?>
	<tr class="form-field term-group-wrap">
		<th scope="row">
			<label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
		</th>
		<td>
			<input type="text" id="alternative_heading" name="alternative_heading"
			       value="<?php echo $alternative_heading; ?>"/>
		</td>
	</tr>
	<?php
}

add_action( 'projects_charity_edit_form_fields', 'projects_charity_edit_meta_fields', 10, 2 );

function projects_charity_save_taxonomy_meta( $term_id, $tag_id ) {
	if ( isset( $_POST['alternative_heading'] ) ) {
		update_term_meta( $term_id, 'alternative_heading', esc_attr( $_POST['alternative_heading'] ) );
	}
}

add_action( 'created_projects_charity', 'projects_charity_save_taxonomy_meta', 10, 2 );
add_action( 'edited_projects_charity', 'projects_charity_save_taxonomy_meta', 10, 2 );
