<?php


add_action( 'admin_head-edit.php', 'addCustomImportButton' );


/**
 * Adds "Import" button on module list page
 */

function addCustomImportButton() {
	global $current_screen;

	// Not our post type, exit earlier
	// You can remove this if condition if you don't have any specific post type to restrict to.
	if ( 'wppwp_projects' != $current_screen->post_type ) {
		return;
	}

	?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            jQuery(jQuery(".wrap h2")[0]).append("<a  id='doc_popup' class='add-new-h2'>Import</a>");
        });
    </script>
	<?php
}

add_action( 'admin_menu', 'add_to_cpt_menu' );

function add_to_cpt_menu() {
	add_submenu_page( 'edit.php?post_type=wppwp_projects', 'Custom Post Type Admin', 'Settings', 'edit_posts', 'my-custom-submenu-page', 'my_custom_submenu_page_callback' );
}

function my_custom_submenu_page_callback() {
	echo '<div class="wrap">';
	echo '<h2>Plugin Settings</h2>'; ?>
    <br>
    <button id="wppwpGiveAll" type="submit" class="button-primary">Download/Update</button>
    <br>    <br>
    <br>


	<?php echo '</div>';

}

if ( isset( $_GET['type'] ) && $_GET['type'] == 'edit_posts' ) {
	include( 'admin/tab/settings.php' );
}

add_action( 'wp_ajax_wppwp_get_all_company_callback', 'wppwp_get_all_company_callback' );
add_action( 'wp_ajax_wppwp_get_all_company_callback', 'wppwp_get_all_company_callback' );


function wppwp_get_all_company_callback() {
	static $page = 645;

	$limit = 50;

	$url = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

	$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

	if ( ! is_wp_error( $request ) ) {
		$respons = json_decode( $request['body'] );

		$total_pages = $respons->meta->total_pages;

		for ( $i = $page; $i ++ <= $total_pages; ) {
			write_all_company_to_DB( $limit, $page ++ );
		}
	} else {
		echo 'error :';
		var_dump( $request );
	}


	die( 'done' );
}


function write_all_company_to_DB( $limit, $page ) {
	global $wpdb;
	$user_id = get_current_user_id();
	$url     = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

	$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

	$campanys          = json_decode( $request['body'] );

	$post_author           = $user_id;
	$post_date_gmt         = current_time( 'mysql' );
	$post_excerpt          = '';
	$post_status           = 'publish';
	$comment_status        = 'close';
	$ping_status           = 'closed';
	$post_password         = '';
	$to_ping               = '';
	$pinged                = '';
	$post_modified_gmt     = current_time( 'mysql' );
	$post_content_filtered = '';
	$post_parent           = 0;
	$guid                  = '';
	$menu_order            = 0;
	$post_type             = 'wppwp_projects';
	$post_mime_type        = '';
	$comment_count         = 0;


	foreach ( $campanys->campaigns as $campany ) {


		$json_campany = json_encode( $campany );
		$post_title   = $campany->name;
		//	$post_content      = ! empty( $campany->description ) ? $campany->description : '';
		$post_name         = $campany->slug;
		$display_start_at  = $campany->display_start_at;
		$display_finish_at = $campany->display_finish_at;
		$start_at          = $campany->start_at;
		$start_date        = new DateTime( $start_at );
		$finish_at         = $campany->finish_at;
		$finish_date       = new DateTime( $finish_at );
		$api_ID            = $campany->id;
		$ID                = preg_replace( '/[^0-9]/', '', $campany->id );

		$country_code = $campany->country_code;
		$banner_url   = $campany->banner_url;
		$status       = $campany->status;
		$donate_url   = $campany->donate_url;
		$funds_raised = $campany->funds_raised;
		$banner_url   = $campany->banner_url;

		$post_content = ! empty( $campany->description ) ? $campany->description : '';


		if ( $status == 'active' ) {
			$post_data = [
				'post_title'   => $post_title,
				'post_content' => $post_content,
				'post_type'    => 'wppwp_projects',
				'post_name'    => $post_name,
				'post_status'  => 'publish',

			];

			$sql = $wpdb->prepare( " INSERT INTO {$wpdb->posts}  VALUES( %d, %d, NOW(), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NOW(), %s, %d, %s, %s, %d, %s, %s, %d )
 ON DUPLICATE KEY UPDATE post_author= $post_author,post_title= '$post_title'",
				$ID,
				$post_author,
				$post_date_gmt,
				$post_content,
				$post_title,
				$post_excerpt,
				$post_status,
				$comment_status,
				$ping_status,
				$post_password,
				$post_name,
				$to_ping,
				$pinged,
				$post_modified_gmt,
				$post_content_filtered,
				$post_parent,
				$guid,
				$menu_order,
				$post_type,
				$post_mime_type,
				$comment_count
			);

			$wpdb->query( $sql );

			if ( $wpdb->insert_id ) {
				$page_id = $ID;

				// save "_wppwp_json_campany" all json campany Object for debugging.
				update_post_meta( $page_id, '_wppwp_json_campany', $json_campany );
				update_post_meta( $page_id, '_wppwp_api_id', $api_ID );

				update_post_meta( $page_id, '_wppwp_display_start_at', $display_start_at );

				update_post_meta( $page_id, '_wppwp_id', $ID );

				update_post_meta( $page_id, '_wppwp_display_finish_at', $display_finish_at );

				update_post_meta( $page_id, '_wppwp_start_at', $start_at );
				update_post_meta( $page_id, '_wppwp_start_at_formated', $start_date->format( "Y-m-d" ) );


				update_post_meta( $page_id, '_wppwp_finish_at', $finish_at );
				update_post_meta( $page_id, '_wppwp_finish_at_formated', $finish_date->format( "Y-m-d" ) );

				update_post_meta( $page_id, '_wppwp_country_code', $country_code );

				update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );

				update_post_meta( $page_id, '_wppwp_status', $status );

				update_post_meta( $page_id, '_wppwp_donate_url', $donate_url );

				update_post_meta( $page_id, '_wppwp_funds_raised', $funds_raised );

				update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );
				$blogtime = current_time( 'mysql' );
				update_post_meta( $page_id, '_wppwp_last_time_update', $blogtime );

			} else {
				$errors['wp_insert_post'] = 'There was an error adding the complaint.';
			}

		}
	}

	return $campanys;


}

//todo add method if need
add_action( 'wp_ajax_wppwp_update_meta_company_callback', 'wppwp_update_meta_company_callback' );
add_action( 'wp_ajax_wppwp_update_meta_company_callback', 'wppwp_update_meta_company_callback' );


function wppwp_update_meta_company_callback() {

	return die( '3333' );
}


add_action( 'wp_ajax_wppwp_update_single_company_callback', 'wppwp_update_single_company_callback' );
add_action( 'wp_ajax_wppwp_update_single_company_callback', 'wppwp_update_single_company_callback' );

function wppwp_update_single_company_callback() {

	$post_id = get_the_ID();

	https://everydayhero.com/api/v2/campaigns/

  var_dump($post_id);
	wp_die();

}